#! /bin/zsh


# 
# 

usage () {
            cat << EOF
Usage: $(basename $0) [options] csvfile
Options:
	--help		This help message.
	--use-teide-csv	FILE.csv
			Use CSV file from TEIDE for teams.
	--groupsfile ensi1a.csv
			Use file to get students group
			(use get-list.sh to download one)
EOF
}

csvfile=""
groupsfile=""
teide_csv=""

while test $# -ne 0; do
    case "$1" in
        "--help"|"-h")
            usage
            exit 0
            ;;
	"--groupsfile")
	    shift
	    groupsfile="$1"
	    ;;
	"--use-teide-csv")
	    shift
	    teide_csv="$1"
	    ;;
        *)
            csvfile="$1"
            ;;
    esac
    shift
done

if [ "$csvfile" = "" ]; then
    echo "please specify csv file on command-line."
    usage
    exit 1
fi

while read -r line; do
    login=$(echo "$line" | cut -d \; -f 4)
    if [ "$login" = "" ]; then
	echo "$line"
	continue
    fi
    if [ "$teide_csv" != "" ]; then
	teamnumber=$(grep ",$login\$" "$teide_csv" | cut -d , -f 2)
	team="$teamnumber"_$(grep "^,$teamnumber," "$teide_csv" | cut -d , -f 5 | tr '\n' '-' | sed 's/-$//')
    else
	team=$(echo *[_-]"$login"[_-]*(^/) | cut -d _ -f 1-2)
    fi
    rem=""
    for teammate in $(echo "$team" | cut -d _ -f 2 | tr '-' ' '); do
	if [ "$teammate" != "$login" ]; then
	    if ! grep -q "^[^;]*;[^;]*;[^;]*;$teammate;" "$csvfile"; then
		if [ -f "$groupsfile" ]; then
		    tail="$tail
$(grep ";$teammate\$" "$groupsfile");$team;vient d'un autre groupe"
		    grep "^[^;]*;[^;]*;[^;]*;$teammate;" "$groupsfile" >&2
                    rem="$teammate = $(grep "^[^;]*;[^;]*;[^;]*;$teammate;" "$groupsfile" | cut -d \; -f 3)"
		else
		    rem="$teammate vient d'un autre groupe"
		fi
	    fi
	fi
    done
    echo "${line};$team;$rem"
done < "$csvfile"

echo "$tail"
