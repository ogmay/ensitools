#! /bin/sh

# Send emails to students based on a list of filenames.

dry_run=no
body=""
opts=""
subject=""
verbose=no
logfile=$(basename $0).log
list_teams=no
muttredirect=""

usage()
{
    cat << EOF
Usage: $(basename $0) [options]
Send email to students based on a list of filenames.

Options:
	--help, -h	this help message
	--body file.txt file containing the body of the message
	--subject, -s SUBJECT
			subject line.
	--bcc ADDR	address to put in bcc.
	--cc ADDR	address to put in cc
	--grades, -g file.csv
			CSV file containing grades (CSV export of Zenith's gradesfiles)
	--emails file.csv
			CSV file containing emails (Zenith's CSV)
	--verbose, -v	be verbose
	-n, --dry-run	don't actually send.
	--batch		non-interactive mode.
	--list-teams	don't send anything, just print the list of teams.

Typical usage:

  $ cd produits_NN/valide/

  # do some cleanup
  # re-tar the students directories
  # say, into NNN_login1-login2_archive-corrige.tar.gz

  $ $(basename $0) --bcc yourself@imag.fr \\
                   --body some-message.txt \\
                   --grades gradefile.csv \\
                   --subject "Corrige TP" *-corrige.tar.gz

Then, $(basename $0) will launch mutt for each team, with a message
prepared to be sent, including the attachment.

gradefile.cvs must be a CSV file containing lines like
student_id1;...;...;...;12
student_id2;...;...;...;19
...

(as exported by Zenith)

some-message.txt can contain @TEAM@ and @GRADE@, they will be
substituted by the actual team name and the grade extracted from
gradefile.

If a .txt file is located next to the file to send as attachment, then
it will be appended to the body of the message. For
example, if sending foo-corrige.zip, then the content of the file
foo-corrige.zip.txt will be inserted.

One use-case it to find the list of remarks with e.g.:

  git grep 'MM: ' | cut -d / -f 1 | sort | uniq > list.txt
  compress-all.sh \$(cat list.txt) --zip --suffix -corrige
  for d in \$(cat list.txt); do (cd \$d; git grep -n 'MM: ' > ../\$d-corrige.zip.txt); done

The script will search for the existence of
~/.batch-send-teiderc
./batch-send-teiderc
and source them as shell-script files (in this order, and before
looking at command-line arguments). They may contain lines like:

bcc=foo
cc=bar
verbose=no
verbose=yes
gradesfile=path/to/file.csv
emailsfile=path/to/emails.csv
dry_run=no
dry_run=yes
batch=yes
batch=no
body=path/to/file.txt
subject="subject line to use"
EOF
}

gradesfile=""
emailsfile=""
search_email=csv
batch="no"

for file in ~/.batch-send-teiderc ./batch-send-teiderc; do
    if [ -e "$file" ]; then
        . "$file"
    fi
done

while test $# -ne 0; do
    case "$1" in
        "--help"|"-h")
            usage
            exit 0
            ;;
        "--body")
            shift
            body="$1"
            ;;
        "--subject"|"-s")
            shift
            subject="$1"
            ;;
        "--bcc")
            shift
            bcc="$1"
            ;;
        "--cc")
            shift
            cc="$1"
            ;;
        "--grades"|"-g")
            shift
            gradesfile="$1"
            ;;
	"--emails")
	    shift
	    emailsfile="$1"
	    search_email=csv
	    ;;
        "--batch")
            batch=yes
            ;;
        "--verbose"|"-v")
            verbose=yes
            ;;
        "-n"|"--dry-run")
            dry_run=yes
            ;;
        "--list-teams")
            list_teams=yes
            ;;
        "--")
            shift
            break
            ;;
        *)
            break
            ;;
    esac
    shift
done

touch "$logfile"

if [ "$batch" = "yes" ]; then
    # redirect mutt's stdin to /dev/null
    # this way, mutt becomes non-interactive.
    muttredirect='< /dev/null'
fi

if [ "$bcc" != "" ]; then
    opts="$opts -b $bcc"
fi

if [ "$cc" != "" ]; then
    opts="$opts -c $cc"
fi

if [ x"$body" = x"" ]; then
    echo "You must specify a text body with --body"
    usage
    exit 1
else
    opts="$opts"
fi

if [ "$gradesfile" != "" ] && [ ! -r "$gradesfile" ]; then
    echo "$gradesfile: can not access file (grades file)"
fi

if [ "$emailsfile" != "" ] && [ ! -r "$emailsfile" ]; then
    echo "$emailsfile: can not access file (email file)"
fi

if [ x"$subject" = x"" ]; then
    echo "You must specify a subject with --subject"
    usage
    exit 1
else
    opts="$opts -s \"$subject\""
fi

if [ "$#" = 0 ]; then
    echo "Please provide archive files on the command line."
    exit 1
fi

case $search_email in
    ldap)
	echo "search_email=ldap not supported at the moment, sorry."
	exit 1
	;;
    csv)
	if [ -z "$emailsfile" ]; then
	    echo "You must specify a value for \$emailsfile."
	    exit 1
	fi
	if ! [ -r "$emailsfile" ]; then
	    echo "Can't read $emailsfile:"
	    ls "$emailsfile"
	    exit 1
	fi
	;;
    *)
	echo "search_email=$search_email not supported, sorry."
	exit 1
	;;
esac

for file in "$@"
do
    # deal with filenames like produits/valide/1234_foo-bar_blabla
    # as well as 1234_foo-bar_bla/blo/blu.txt
    basefile=$(echo "$file" | sed 's@.*/\([0-9][0-9]*_\)@\1@')
    team=$(echo "$basefile" | sed 's/^\([0-9]*_[^_]*\)_.*$/\1/')
    team_number=$(echo "$basefile" | cut -d _ -f 1)
    if [ "$list_teams" = "yes" ]; then
        echo "$team"
        continue
    fi

    no=$(echo "$team" | sed 's/^\([0-9]*\)_.*$/\1/')
    logins=$(echo "$team" | sed 's/^[0-9]*_\([^_]*\)\(_.*\|\)$/\1/')
    if echo "$logins" | grep -q '^[0-9]' ; then
        echo "Error while extracting logins from ${basefile}:"
	echo "team=$team"
	echo "no=$no"
	echo "logins=$logins"
        exit 1
    fi
    logins=$(echo "$logins" | sed 's/-/ /')
    student_ids=""
    case $search_email in
	ldap)
	    emails=$(ssh pcserveur.ensimag.fr 'for login in '"$logins"'; \
                         do ldapsearch -H ldap://ensildap.imag.fr -x -b dc=ensimag,dc=imag,dc=fr uid="$login"; \
                         done' \
		| grep "^mail:" | sed "s/^mail: *//" | tr '\n' ' ')
	    ;;
	csv)
	    emails=""
	    for login in $logins
	    do
		thisemail="$(grep "^[^;]*;$login;" $emailsfile | cut -d \; -f 5)"
		if ! printf '%s' "$thisemail" | grep -q '^[a-zA-Z0-9.-]*@[a-zA-Z.-]*$'; then
		    echo "Invalid email for $login : $thisemail"
		    echo "Found in $emailsfile"
		    exit 1
		fi
		emails="$emails $thisemail"
		thisid="$(grep "^[^;]*;$login;" $emailsfile | cut -d \; -f 7)"
		if ! printf '%s' "$thisid" | grep -q '^[0-9][0-9]*$'; then
		    echo "Invalid student ID $thisid"
		    exit 1
		fi
		student_ids="$student_ids $thisid"
	    done
	    ;;
	*)
	    echo "Invalid value $search_email for search_email"
	    exit 1
    esac
    if echo "$emails" | grep -q @ ; then
        :
    else
        echo "Error while extracting adresses (got $emails)"
        exit 1
    fi

    if [ x"$verbose" = x"yes" ]; then
        echo "File $file"
        echo "Team $team no $no"
        echo "Mail to be sent to $emails"
    fi
    bodytmp=${body%*.txt}-${team}.txt
    if [ x"$gradesfile" != x"" ]; then
	oldgrade=''
	for id in $student_ids; do
	    grade=$(grep "^$id;" "$gradesfile" | uniq | cut -d \; -f 5)
            echo "Grade for $id ($logins): $grade."
	    if [ $(echo "$grade" | wc -l) -ne 1 ]; then
		echo "Number of grade for team should be exactly 1"
		exit 1
	    fi
	    if [ "$oldgrade" != "" ]; then
		if [ "$oldgrade" != "$grade" ]; then
		    echo "Different grades $oldgrade and $grade for logins in team $logins."
		    exit 1
		fi
	    fi
	    oldgrade=$grade
	done
        sed -e "s?@GRADE@?${grade}?" \
            -e "s?@TEAM@?${team}?" \
        < "$body" > "$bodytmp" || exit 1
    else
        echo "no grades file provided"
        cp "$body" "$bodytmp"
    fi
    if [ -f "$file".txt ]; then
	echo >> "$bodytmp"
	cat "$file".txt >> "$bodytmp"
    fi
    cmd="mutt $opts -i ${bodytmp} -a \"$file\" -- $emails"
    if [ x"$dry_run" = x"no" ]; then
        echo $cmd
        (date; echo $cmd)         >> "$logfile"
        eval $cmd  $muttredirect 2>> "$logfile"
        (echo "status=$?"; echo)  >> "$logfile"
    else
        echo "dry run: " $cmd
    fi
done

grep 'status=[^0]' -A5 "$logfile" || true
