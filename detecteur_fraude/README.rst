Utilisation du détecteur
========================

#. installer les pré-requis : unzip, rar, gzip + tar, bunzip2, ssdeep

#. télécharger l'ensemble des projets de TEIDE (produits_xxx.zip)

#. utiliser decompress.pl produits_xxx.zip pour décompresser les fichiers.

   il est également possible de tout récupérer manuellement
   néanmoins tous les projets doivent se trouver dans le meme
   répertoire où chaque sous-répertoire correspond à un groupe
   d'étudiants

#. rajouter dans le répertoire produits_xxx un sous-répertoire
   "provided" contenant l'ensemble des fichiers fournis par
   l'enseignant

#. rajouter dans le répertoire produits_xxx un sous-répertoire
   "internet" contenant des éventuels fichiers récupérés d'internet

#. exécuter detect.pl produits_xxx (ou detect.pl -i produit_xxx
   pour effectuer une vérification manuelle)

Plus d'information
==================

Ce détecteur de fraudes est écrit par Frédéric Wagner.
