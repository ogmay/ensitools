#!/usr/bin/perl -w

use strict;
use File::Find;

my $basedir = $ARGV[$#ARGV];
#turn to 1 for debugging info
my $verbose = 0;
#files shorter than the limit will not be checked
my $limit = 10;

my @watched_files = (".c", ".cc", ".cpp", ".java", ".pl", ".py", ".adb");
my @source_files;
push @source_files, $_ foreach(@watched_files);
push @source_files, uc($_) foreach(@watched_files);

my $arg;
my $interactive = 0;

die "usage: detect.pl [-iv] project_dir\n -i: interactive mode (shows diff)\n -v: verbose mode\n" unless exists $ARGV[0];

my $parsing_limit = 0;
foreach $arg (@ARGV) {
	if ($parsing_limit) {
		$limit = $arg;
		$parsing_limit = 0;
	}
	$interactive = 1 if ($arg eq "-i");
	$verbose = 1 if ($arg eq "-v");
	$parsing_limit = 1 if ($arg eq "-l");
	die "usage: detect.pl [-iv] [-l size] project_dir\n -i: interactive mode (shows diff)\n -v: verbose mode\n -l size : limit check to files larger than size lines\n" if ($arg eq "-h" or $arg eq "--help");
}

#check for the few programs we need
$_ = `which ssdeep`;
die "ssdeep is not installed!" unless /ssdeep/;
$_ = `which gvim`;
if ($interactive == 1 and not /gvim/) {
	print "*disabling interactive mode: \"gvim\" not found*\n";
	$interactive = 0;
}
$_ = `which dialog`;
if ($interactive == 1 and not /dialog/) {
	print "*disabling interactive mode: \"dialog\" not found*\n";
	$interactive = 0;
}

print "source files extensions monitored : ".join(' ', @source_files)."\n";
print "files shorter that $limit will not be monitored\n";

#get a list of students
opendir(DIR, $basedir) or die "can't open initial directory: $!";

my @students;
while($_ = readdir(DIR)) {
	my $tmp = "$basedir/$_";
	next unless -d $tmp;
	next if /^\./;
	push @students, $_;
}

closedir(DIR);

print "students : @students\n" if $verbose;

#compute a hash of lines of provided files
my %provided;
if (-d "$basedir/provided") {
	print "parsing provided files\n" if $verbose;
	find(\&compute_hash, "$basedir/provided");
}


#find code
print "searching for code\n" if $verbose;
chdir($basedir);

my @code;
find(\&find_code, @students);

#build files with provided elements removed
print "building files to compare\n" if $verbose;
my @code2;
my $file;
my $file_number = 0;
foreach $file (@code) {
	my $newfile = $file . "_detect";
	open(ORIGINAL, "< $file") or die "problem opening $file: $!";
	open(MODIFIED, "> $newfile");

	my $counter = 0;
	while(<ORIGINAL>) {
		my $no_white = $_;
		$no_white =~ s/\s//g;
		if (not $provided{$no_white}) {
			print MODIFIED $_;
			$counter++;
		}
	}

	close(ORIGINAL);
	close(MODIFIED);
	if ($counter < $limit) {
		unlink $newfile;
	} else {
		push @code2, "\"$newfile\"";
	}
	$file_number++;
	update_progress(100 * $file_number / $#code) if (($file_number % 3) == 0) and $verbose;
}

$_ = $#code2 + 1;
print "\ntotal number of files: $_\n" if $verbose;

#do the comparisons
print "starting comparisons\n" if $verbose;
my @matches;

open(EXEC, "ssdeep -d @code2 |");
while(<EXEC>) {
	push @matches, $_ if /matches/;
}
close(EXEC);

print "sorting results\n" if $verbose;
my @comparisons;
foreach(sort compare @matches) {
	my $student;
	my $bool = 0;
	foreach $student (@students) {
		$bool = 1 if /\Q$student\E.+\Q$student\E/;	
	}
	next if $bool == 1;
	print $_ unless $interactive;
	s/matches//;
	s/\(\d+\)//;
	push @comparisons, $_;
}

print "no frauds detected!\n" if $#comparisons == -1;

if ($interactive and $#comparisons != -1) {
	my $choice;
	my $tmp = $#comparisons + 1;
	$choice = system("dialog --yesno \"$tmp possible frauds. see them ?\" 10 40");
	if ($choice == 0) {
		foreach(@comparisons) {
			print "comparing $_\n";
			`gvim -d $_`;
		};
	}
}

sub compare {
	$a =~/\((\d+)\)/;
	my $i = $1;
	$b =~/\((\d+)\)/;
	my $j = $1;
	return $j <=> $i;
}

sub find_code {
	return unless check_source($_);
	return unless -f $_;
	return if $File::Find::name =~/__MACOSX/;
	return if $File::Find::name =~/provided\//;
	push @code, $File::Find::name if -f $_;
}

sub compute_hash {
	return unless check_source($_);
	return unless -f $_;
	open(FILE, "< $_");
	while(<FILE>) {
		my $no_white = $_;
		$no_white =~ s/\s//g;
		$provided{$no_white} = 1;
	}
	close(FILE);
}

sub check_source {
	our ($string) = @_;
	foreach(@source_files) {
		return 1 if $string =~/\Q$_\E$/;
	}
	return 0;
}

sub update_progress {
        our ($percent) = @_;
        print "\r";
        print "[";
        foreach(0..19) {
                if ($_ < $percent / 5) {
                        print "*";
                } else {
                        print ".";
                }
        }
        print "]";
        printf "%.0f", $percent;
        print "% completed";
}

