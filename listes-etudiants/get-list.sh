#! /bin/sh

course=3MMUNIX
groups="TD_G1 TD_G2 TD_G3 TD_G4 TD_G5 TD_G6 TD_G7 TD_G8"
extra_groups=""
# 3MMEPSS1_2014_S1_TD_G1:EPS"
year=2016
semester=S1
output=ensi1a.csv
skip_download=

usage () {
            cat << EOF
Usage: $(basename $0) [options]
Options:
	--help	
                This help message.
        --year name
                Year (e.g. 2012)
                Default is ${year}
        --course name
                Course name on Zenith.
                Default is ${course}
	--groups "G1 G2 G3 ..."
		Groups (on Zenith) to include.
                Default is "${groups}"
        --output filename
                Default is ${output}
	--no-semester
		Don't include semester in group name
Generates a file based on ZENITH data.
EOF
}

user="$LOGNAME"
no_semester=

while test $# -ne 0; do
    case "$1" in
        "--help"|"-h")
            usage
            exit 0
            ;;
	"--year")
	    shift
	    year=$1
	    ;;
	"--course")
	    shift
	    course=$1
	    ;;
	"--groups")
	    shift
	    groups=$1
	    ;;
	"--output"|"-o")
	    shift
	    output="$1"
	    ;;
	"--username"|"--user")
	    shift
	    user="$1"
	    ;;
	"--skip-download")
	    skip_download=t
	    ;;
	"--no-semester")
	    no_semester=t
	    ;;
        *)
            echo "unrecognized option $1"
            usage
            exit 1
            ;;
    esac
    shift
done


get_group () {
    short=$2
    long=$3
    if [ -z "$short" ]; then
	short=$1
    fi
    if [ -z "$long" ]; then
	if [ -n "$no_semester" ]; then
	    long="${course}_$1_${year}"
	else
	    long="${course}_${year}_${semester}_$1"
	fi
    fi 
    url="https://intranet.ensimag.fr/Zenith2/Groupe/getGroupsCoursCsv?name=$long"
    if [ "$skip_download" = t ]; then
	printf '%s\n' "cat 'csv/$short'.csv" >&2
	cat "csv/$short.csv"
    else
	printf '%s\n' "wget --ca-certificate=intranet.ensimag.fr.cer --ask-password --user='$user' '$url' -O -" >&2
	wget --ca-certificate=intranet.ensimag.fr.cer --user="$user" "$url" -O -
    fi | \
	grep '^ *[0-9]' | \
	sed 's/ *; */;/g' | \
	awk -F";" '{ print $4";"$3";'"$short"';"$2";"$7}'
}

for groupe in $groups; do
    get_group "$groupe" | tee "$groupe".csv
done > "${output%.csv}"-raw.csv

cp "${output%.csv}"-raw.csv "$output"

for extra in $extra_groups; do
    zenith=$(printf '%s' "$extra" | cut -f 1 -d :)
    short=$(printf '%s' "$extra" | cut -f 2 -d :)
    printf 'Adding %s as %s\n' "$zenith" "$short"
    get_group "$zenith" "$short" "$zenith" > "$short".csv
    while read line; do
	login=$(printf '%s' "$line" | cut -d \; -f 4)
	perl -pi -e 's/([^;]*;[^;]*);[^;]*;'$login';/\1;'$short';'$login';/g' "$output" 
    done < "$short".csv
done

# ZENITH était en latin1 avant 2013
# recode -t latin1..utf8 "$output"

wc -l "$output"
