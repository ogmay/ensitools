#! /bin/sh

usage () {
            cat << EOF
Usage: $(basename $0) [options] N
Options:
	--help	This help message.
	--salle Nom de salle (D200, E300, ...)
EOF
}

N=undef
salle=undef

while test $# -ne 0; do
    case "$1" in
        "--help"|"-h")
            usage
            exit 0
            ;;
        "--salle")
	    shift
	    salle="$1"
            ;;
	"--dispo")
	    AFFICHE_DISPO=t
	    ;;
	*)
	    N=$1
	    ;;
    esac
    shift
done

if [ "$N" = "undef" ]; then
    echo "ERROR: Merci d'entrer un nombre de machines (N)"
    usage
    exit 1
fi

if [ "$salle" = "undef" ]; then
    echo "ERROR: Merci d'entrer une salle (--salle XXX)"
    usage
    exit 1
fi

if [ ! -f "./postes-$salle.txt" ]; then
    echo "ERROR: Pas de liste de poste pour la salle $salle, désolé."
    usage
    exit 1
fi

disp=$(grep -c -v '^ *#' "./postes-$salle.txt")

if [ "$N" -gt $disp ]; then
    echo "ERROR: Pas assez de postes disponibles en salle $salle."
    echo "demandé : $N"
    echo "dispo : $disp" 
    usage
    exit 1
fi

if [ "$AFFICHE_DISPO" = t ]; then
    grep -v '^ *#' "./postes-$salle.txt" | grep . | tail -n +"$((N + 1))" | sed "s/^/$salle /"
else
    grep -v '^ *#' "./postes-$salle.txt" | grep . | head -n "$N"
fi
