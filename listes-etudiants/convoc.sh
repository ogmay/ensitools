#!/bin/bash

. config.sh

echo 'Etudiant;Horaire;Salle' > convoc.csv
awk -F";" '{print $1" "$2";"$5";"$7}' liste.csv | sort >> convoc.csv
./csv2html.sh --h1 --th \
    --title "Convocation $intitule" \
    --comment "$commentaire" \
    --css style.css convoc.csv > convoc.html
