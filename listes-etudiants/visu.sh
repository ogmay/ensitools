#!/bin/bash

. config.sh

list="$1"
if [ "$list" = "" ]; then
    echo "ERROR: merci de preciser un fichier csv en argument"
    exit 1
fi

for heure in "$session1-$session1_fin" "$session2-$session2_fin"
do
    echo "### $heure"
    grep "$heure" $1 | awk -F";" '{ print $6 }' | ./visuexampc
done
