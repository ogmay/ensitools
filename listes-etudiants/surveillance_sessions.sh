#!/bin/bash

erreur () {
    echo "ERREUR $1"
    exit 1
}

function emargement() {
    local salle=$1
    local horaire=$2
    echo "Nom;Prenom;Machine;Signature Etudiant" > emargement_${salle}_${horaire}.csv
#    echo  >> emargement_${salle}_${horaire}.csv 
    grep "${salle}" liste.csv | grep "${horaire}-" | \
	awk -F";" '{ print $1";"$2";"$6";   " }' | \
        sort >> emargement_${salle}_${horaire}.csv 
}


### ATTENTION: le html est reglé pour l'impression via le firefox 
###   de galise.imag.fr (a Verimag).
function trombino() {
    local salle=$1
    local horaire=$2
    local fich=plan_${salle}_${horaire}.html
    local url="http://intranet.ensimag.fr/zenith/photos/"
    local -i count=0

    echo '<?xml version="1.0" encoding="UTF-8"?>' > ${fich}
    echo "<html><head>
  <title>SESSION ${salle} ${horaire}</title>
  <meta charset=\"utf-8\">
</head>" >> ${fich}
    echo "<body>" >> ${fich}

    echo "<H3 align=center>PLAN SESSION ${salle} ${horaire}</H3>" >> ${fich}
    echo "<br><br>" >> ${fich}
    echo "<p align=center><pre>" >> ${fich}
    grep "ensi" emargement_${salle}_${horaire}.csv | \
	awk -F";" '{print $3}' | ./visuexampc >> ${fich} || erreur emargement_${salle}_${horaire}.csv
    echo "</pre></p>" >> ${fich}
    echo "<br><br><br>" >> ${fich}

    echo "<table>" >> ${fich}
    echo "<tr>" >> ${fich}
    
    for lig in $(grep "ensi" emargement_${salle}_${horaire}.csv | \
	sort -t";" -k4 | sed -e "s/ /_/g") ; do
      nom=$(echo ${lig} | awk -F ";" '{ print $1 }' | sed -e "s/_/ /g")
      prenom=$(echo ${lig} | awk -F ";" '{ print $2 }' | sed -e "s/_/ /g")
      # gpe=$(echo ${lig} | awk -F ";" '{ print $3 }' | sed -e "s/_/ /g")
      mach=$(echo ${lig} | awk -F ";" '{ print $3 }' | sed -e "s/_/ /g")
      if loginl=$(grep "${nom};${prenom}" liste.csv) ; then
	  if [ ${count} -eq 3 ] ; then
	      echo "</tr><tr>" >> ${fich}
	      count=0
	  fi
	  login=$(echo ${loginl} | awk -F ";" '{print $4}')
	  echo "<td><table>" >> ${fich}
	  echo "<tr><td align=center>${mach}</td></tr>" >> ${fich}
	  echo "<tr><td align=center><small>${nom} ${prenom}</small></td></tr>" >> ${fich}
          echo "<tr><td align=left><img width=200 height=150 src=\"${url}${login}.jpg\"></td></tr>" >> ${fich}
	  echo "<tr><td><br><br></td></tr>" >> ${fich}
	  echo "</table></td>" >> ${fich}
	  count+=1
      else
	  echo "ATTENTION ${nom} ${prenom} => pas de login !"
	  exit 1
      fi
    done
    echo "</tr>" >> ${fich}
    echo "</table>" >> ${fich}
    echo "</body></html>" >> ${fich}
}

# MAIN

gnatmake visuexampc

. config.sh

for salle in $salles ; do
    for horaire in $session1 $session2 ; do
	echo "generation de plan_${salle}_${horaire}.html et emargement_${salle}_${horaire}.csv"
	emargement ${salle} ${horaire}
	trombino ${salle} ${horaire} || exit 1
    done
done
