with Ada.Text_Io, Ada.Command_Line ;
use Ada.Text_Io, Ada.Command_Line ;

procedure GenerePoste is

   ErreurUsage, ErreurContrainte: exception ;

   procedure Usage is
   begin
      Put_Line("*** Mode d'emploi:") ;
      Put_Line(Command_Name & " <prefixe> <suffixe-deb> <suffixe-fin> <pas> <num-etudiant>") ;
      Put_Line("prefixe: chaine de caractere") ;
      Put_Line("autres arg.: entiers positifs") ;
      raise ErreurUsage ;
   end ;

   procedure Contrainte is
   begin
      Put_Line("Impossible de satisfaire la contrainte") ;
      raise ErreurContrainte ;
   end ;

   function NomSuffixe(S: Positive) return String is
   begin
      if S < 100 then
         declare
            Res: String(1..2) ;
         begin
            -- zero en tête pour les machines < 100
            Res(1):= Character'Val(S / 10 + Character'Pos('0')) ;
            Res(2):= Character'Val(S mod 10 + Character'Pos('0')) ;
            return Res ;
         end ;
      else
         return Positive'Image(S)(2..4);
      end if;
   end NomSuffixe;

   Deb,Fin, Pas, Num: Positive ;
   Cour: Positive ;
begin
   if Argument_Count /= 5 then
      Put_Line("ERREUR: commande appelee avec "
                 & Natural'Image(Argument_Count) &
                 " arguments (5 attendus).");
      Usage ;
   end if ;
   begin
      Deb:=Positive'Value(Argument(2)) ;
      Fin:=Positive'Value(Argument(3)) ;
      Pas:=Positive'Value(Argument(4)) ;
      Num:=Positive'Value(Argument(5)) ;
   exception
      when Constraint_Error =>
         Put_Line("ERREUR: Un des argument n'est pas un entier positif");
         Usage ;
   end ;
   if Deb+Pas*(Num-1) > Fin then
      Contrainte ;
   end if ;
   for I in 0..Num-1 loop
      Cour:=Deb+Pas*Integer(Float'Floor(Float(I*(Fin-Deb))/Float(Pas*(Num-1)))) ;
      Put_Line(Argument(1) & NomSuffixe(Cour)) ;
   end loop ;
end ;
