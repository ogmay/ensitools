all: plans-emargements convoc.csv liste.csv pdf

MODE = liste-sh

# Some script use badly named temporary files => parallel build breaks
# them.
.NOTPARALLEL:

config.mk: config.sh Makefile
	@echo "# Warning: generated file (based on $<). Do not edit" > $@
	sed 's/"//g' $< >> $@
include config.mk

# Liste des étudiants brute, depuis ZENITH (i.e. via le web)
ensi1a.csv: config.mk
	./get-list.sh --course $(matiereZenith) --year $(year) --user $(zenithUser)

# Liste avec affectation etudiants <-> horaire <-> machine
# Cette étape demande une adaptation de poste-sessionX.sh pour choisir
# les postes en fonction du nombre d'étudiants.
# Eventuellement, il faut aussi adapater "liste.sh"
ifeq ($(MODE),liste-sh)
liste.csv: ensi1a.csv config.mk liste.sh poste-session*.sh
	./liste.sh
else
liste.csv: ./pergroup-list.sh
	./pergroup-list.sh > $@
endif

# Convocation à envoyer à la scolarité pour affichage.
convoc.csv convoc.html: liste.csv
	./convoc.sh
convoc.pdf: convoc.csv
	./csv2pdf.sh --title "Convocation $(intitule)" --comment "$(commentaire)" $< -o $@

# Pretty-print en HTML pour impression et distribution aux
# surveillants.
PLANS_SESSION1=$(salles:%=plan_%_$(session1).html)
PLANS_SESSION2=$(salles:%=plan_%_$(session2).html)
PLANS=$(PLANS_SESSION1) $(PLANS_SESSION2)

# Feuilles d'émargement, à ouvrir dans un tableur pour impression.
EMARGEMENTS_SESSION1=$(salles:%=emargement_%_$(session1).csv)
EMARGEMENTS_SESSION2=$(salles:%=emargement_%_$(session2).csv)
EMARGEMENTS=$(EMARGEMENTS_SESSION1) $(EMARGEMENTS_SESSION2)
EMARGEMENTS_HTML=$(EMARGEMENTS:%.csv=%.html)
EMARGEMENTS_PDF=$(EMARGEMENTS:%.csv=%.pdf)

# Découpage de liste.csv et pretty-print en HTML
.PHONY: plans-emargements pdf
plans-emargements: $(PLANS) $(EMARGEMENTS) $(EMARGEMENTS_HTML)
pdf: $(EMARGEMENTS_PDF) convoc.pdf

$(PLANS) $(EMARGEMENTS): liste.csv
	./surveillance_sessions.sh

# Version formattée des feuilles d'émargement, à imprimer directement.
# Au choix, PDF ou HTML
emargement_%.html: emargement_%.csv
	./csv2html.sh --title "Emargement $*" --h1 --th --css style.css $< > $@

# Nécessite csv2latex : https://launchpad.net/csv2latex
emargement_%.pdf: emargement_%.csv
	./csv2pdf.sh --comment "$(intitule)" --title "Emargement $$(echo $* | sed 's/_/ /g')" --sed 's/|l|}$$/|p{2.5cm}|}/' --nopage $<

# Nettoyage
.PHONY: clean realclean
clean:
	-$(RM) $(PLANS) $(EMARGEMENTS) $(EMARGEMENTS_HTML) $(EMARGEMENTS_PDF) \
		liste.csv convoc.csv convoc.html convoc.pdf config.mk

realclean: clean
	-$(RM) ensi1a.csv *.o *.ali *~ genereposte visuexampc *.tex *.aux *.log

# documentation
include ../Makefile.common

all: doc

doc: README.html

%.html: %.rst Makefile ../style.css
	rst2html $< --stylesheet-path=../style.css \
		--embed-stylesheet \
		> $@
