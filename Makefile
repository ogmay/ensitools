include Makefile.common

doc:
	cd teide-tools/ && $(MAKE) doc
	cd zenith-tools/ && $(MAKE) doc
	cd detecteur_fraude/ && $(MAKE) doc
	cd beamer/ && $(MAKE) doc
	cd latex/ && $(MAKE) doc
	cd squel/ && $(MAKE) doc
	cd listes-etudiants/ && $(MAKE) doc

$(WWW)/%: %
	mkdir -p $$(dirname $@)
	cp $< $@

WEB_SRCS=index.html \
	squel/README.html squel/generer-squelette.pl \
	teide-tools/README.html \
	zenith-tools/README.html \
	detecteur_fraude/README.html \
	beamer/ensislides/README.html \
	latex/ensitd/README.html \
	beamer/theme_ensimag/README.html \
	beamer/theme_ensimag/example.pdf \
        listes-etudiants/README.html \
	ensitools.tar.gz style.css \
	chamilo/index.html \
	chamilo/extraitDescRefEns \
	${wildcard chamilo/*.meta } \
	${wildcard *.meta} ${wildcard latex/*.meta} ${wildcard beamer/*.meta} \
	${wildcard *.link} \
        .htmllistignore

WEB_TARGETS=$(WEB_SRCS:%=$(WWW)/%)

web: doc ensitools.tar.gz
	$(MAKE) $(WEB_TARGETS)
	cd $(WWW) && htmllist.sh --recursive

ensitools.tar.gz: force
#	if git diff HEAD | grep .; then echo "Local changes, please, commit first."; exit 1; fi
	git archive --prefix=ensitools/ --format tar HEAD | gzip > $@

install: install-teide-tools install-squel install-zenith-tools \
         install-detecteur_fraude install-beamer
	git push www master
	git push origin master
	$(MAKE) web

install-%:
	cd $* && $(MAKE) install


.PHONY: force
force:
